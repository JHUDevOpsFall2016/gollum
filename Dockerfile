FROM ruby
RUN apt-get -y update && apt-get -y install libicu-dev
VOLUME /wiki
WORKDIR /wiki
ADD ./ /wiki/
RUN gem install github-markdown org-ruby
RUN cd /wiki && bundle install
ENTRYPOINT ["bundle","exec","bin/gollum", "--port", "80"]
EXPOSE 80